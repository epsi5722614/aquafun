# Stage 1: Build frontend
FROM node:20 AS build-frontend
WORKDIR /aquafun-client
COPY aquafun-client/package*.json ./
RUN npm install
COPY aquafun-client/ ./
RUN npm run build -- --output-path=dist

# Stage 2: Build backend
FROM node:20 AS build-backend
WORKDIR /aquafun-server
COPY aquafun-server/package*.json ./
RUN npm install
COPY aquafun-server/ ./
RUN npm run build

# Stage 3: Final image
FROM node:20
WORKDIR /
COPY --from=build-frontend /aquafun-client/dist ./aquafun-client/dist
COPY --from=build-backend /aquafun-server/dist ./aquafun-server/dist
COPY aquafun-server/package*.json ./aquafun-server/
RUN npm install --production --prefix ./aquafun-server
EXPOSE 3000
CMD ["node", "aquafun-server/dist/index.js"]
