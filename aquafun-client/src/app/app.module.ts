import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AquariumsComponent } from './aquariums/aquariums.component';
import { FishComponent } from './fish/fish.component';
import { AquariumService } from './aquarium.service';

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    AquariumsComponent,
    FishComponent
  ],
  providers: [AquariumService],
  bootstrap: [] // Assurez-vous de ne pas avoir de composants ici si vous utilisez `bootstrapApplication`
})
export class AppModule { }
