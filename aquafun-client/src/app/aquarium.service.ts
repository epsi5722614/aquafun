import { Injectable } from '@angular/core';
import { Aquarium, Fish } from './aquarium.model';

@Injectable({
  providedIn: 'root'
})
export class AquariumService {
  private aquariums: Aquarium[] = [];
  private fishList: Fish[] = [];

  addAquarium(name: string) {
    this.aquariums.push({ name, fish: [] });
  }

  getAquariums() {
    return this.aquariums;
  }

  addFishToAquarium(aquarium: Aquarium, fish: Fish) {
    aquarium.fish.push(fish);
  }

  addFish(name: string) {
    this.fishList.push({ name });
  }

  getFishList() {
    return this.fishList;
  }
}
