import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AquariumsComponent } from './aquariums.component';
import { AquariumService } from '../aquarium.service';
import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FishComponent } from '../fish/fish.component';
import { FormsModule } from '@angular/forms';

describe('AquariumsComponent', () => {
  let component: AquariumsComponent;
  let fixture: ComponentFixture<AquariumsComponent>;
  let aquariumService: any;

  beforeEach(async () => {
    aquariumService = jasmine.createSpyObj('AquariumService', [
      'getAquariums',
      'addAquarium',
      'getFishList',
      'addFishToAquarium',
    ]);

    await TestBed.configureTestingModule({
      imports: [FormsModule, AquariumsComponent, FishComponent],
      providers: [{ provide: AquariumService, useValue: aquariumService }],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();

    fixture = TestBed.createComponent(AquariumsComponent);
    component = fixture.componentInstance;
  });

  beforeEach(() => {
    aquariumService.getAquariums.and.returnValue([
      { name: 'Tropical', fish: [{ name: 'Goldfish' }] },
    ]);
    aquariumService.getFishList.and.returnValue([{ name: 'Test' }]); // Mock pour getFishList
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display a list of aquariums', () => {
    const aquariumElements = fixture.debugElement.queryAll(
      By.css('.left ul > li')
    );
    expect(aquariumElements[0].nativeElement.textContent).toContain('Tropical');
  });

  it('should display the fish in the aquariums', () => {
    const fishElements = fixture.debugElement.queryAll(
      By.css('.left ul > li ul > li')
    );
    expect(fishElements.length).toBe(1);
    expect(fishElements[0].nativeElement.textContent).toContain('Goldfish');
  });

  it('should add an aquarium', () => {
    const input = fixture.debugElement.query(By.css('input')).nativeElement;
    const button = fixture.debugElement.query(By.css('button')).nativeElement;

    input.value = 'Freshwater';
    input.dispatchEvent(new Event('input'));
    button.click();

    expect(aquariumService.addAquarium).toHaveBeenCalledWith('Freshwater');
  });

  it(`should have a 'app-fish' component`, () => {
    const debugElement = fixture.debugElement.query(By.css('app-fish'));
    expect(debugElement).toBeTruthy();
  });
});
