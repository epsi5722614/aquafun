// src/app/aquariums/aquariums.component.ts
import { Component, OnInit } from '@angular/core';
import { AquariumService } from '../aquarium.service';
import { Aquarium } from '../aquarium.model';
import { FishComponent } from "../fish/fish.component";
import { NgFor } from '@angular/common';

@Component({
    selector: 'app-aquariums',
    standalone: true,
    templateUrl: './aquariums.component.html',
    styleUrls: ['./aquariums.component.css'],
    imports: [FishComponent, NgFor]
})
export class AquariumsComponent implements OnInit {
  aquariums: Aquarium[] = [];

  constructor(private aquariumService: AquariumService) {}

  ngOnInit() {
    this.aquariums = this.aquariumService.getAquariums();
  }

  addAquarium(name: string) {
    if (name) {
      this.aquariumService.addAquarium(name);
    }
  }
}
