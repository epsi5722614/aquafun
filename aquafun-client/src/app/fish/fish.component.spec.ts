import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FishComponent } from './fish.component';
import { AquariumService } from '../aquarium.service';
import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('FishComponent', () => {
  let component: FishComponent;
  let fixture: ComponentFixture<FishComponent>;
  let aquariumService: any;

  beforeEach(async () => {
    aquariumService = jasmine.createSpyObj('AquariumService', [
      'getFishList',
      'getAquariums',
      'addFishToAquarium',
      'addFish',
    ]);

    await TestBed.configureTestingModule({
      imports: [FishComponent],
      providers: [{ provide: AquariumService, useValue: aquariumService }],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();

    fixture = TestBed.createComponent(FishComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display a list of fish', () => {
    // Arrange
    const fishList = [{ name: 'Goldfish' }, { name: 'Betta' }];
    aquariumService.getFishList.and.returnValue(fishList);

    // Act
    component.ngOnInit();
    fixture.detectChanges();

    // Assert
    const fishElements = fixture.debugElement.queryAll(By.css('li'));
    expect(fishElements.length).toBe(fishList.length);
    expect(fishElements[0].nativeElement.textContent).toContain('Goldfish');
    expect(fishElements[1].nativeElement.textContent).toContain('Betta');
  });
});
