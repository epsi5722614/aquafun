import { Component, OnInit } from '@angular/core';
import { AquariumService } from '../aquarium.service';
import { Fish, Aquarium } from '../aquarium.model';
import { NgFor } from '@angular/common';

@Component({
  selector: 'app-fish',
  standalone: true,
  imports: [NgFor],
  templateUrl: './fish.component.html',
  styleUrls: ['./fish.component.css']
})
export class FishComponent implements OnInit {
  fishList: Fish[] = [];
  aquariums: Aquarium[] = [];

  constructor(private aquariumService: AquariumService) {}

  ngOnInit() {
    this.fishList = this.aquariumService.getFishList();
    this.aquariums = this.aquariumService.getAquariums();
  }

  addFish(name: string, aquariumName: string) {
    if (name && aquariumName) {
      const aquarium = this.aquariums.find(a => a.name === aquariumName);
      if (aquarium) {
        const newFish = { name };
        this.aquariumService.addFishToAquarium(aquarium, newFish);
        this.fishList.push(newFish); // Optional: to also show fish in the fish list
      }
    }
  }
}
