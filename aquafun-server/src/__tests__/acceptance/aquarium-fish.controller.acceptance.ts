import { Client, expect } from '@loopback/testlab';
import { AquafunServerApplication } from '../..'; 
import { setupApplication } from './test-helper'; 
import { AquariumRepository } from '../../repositories';
import { FishRepository } from '../../repositories';

describe('AquariumFishController', () => {
  let app: AquafunServerApplication;
  let client: Client;
  let aquariumRepo: AquariumRepository;
  let fishRepo: FishRepository;

  before(async () => {
    ({ app, client } = await setupApplication());
    aquariumRepo = await app.getRepository(AquariumRepository);
    fishRepo = await app.getRepository(FishRepository);
  });

  after(async () => {
    await app.stop();
  });

  beforeEach(async () => {
    // Nettoyer les données avant chaque test
    await cleanUp();
  });

  async function cleanUp() {
    // Supprimer tous les aquariums et poissons créés pour chaque test
    await aquariumRepo.deleteAll();
    await fishRepo.deleteAll();
  }

  it('creates a fish in an aquarium via POST /aquariums/{id}/fish', async () => {
    // Créer un aquarium pour le test
    const aquarium = await aquariumRepo.create({ name: 'Test Aquarium' });
  
    const fishData = { name: 'Goldfish'};
  
    // Effectuer la requête HTTP POST pour créer un poisson dans l'aquarium
    const res = await client
      .post(`/aquariums/${aquarium.id}/fish`)
      .send(fishData)
      .expect(200);
    
    // Vérifier que la réponse contient les données attendues du poisson créé
    expect(res.body).to.containEql(fishData);
  
    // Vérifier que le poisson a bien été créé dans la base de données
    const createdFish = await fishRepo.findById(res.body.id);
    expect(createdFish).to.not.be.null();
    expect(createdFish!.name).to.equal(fishData.name);
    expect(createdFish!.aquariumId).to.equal(aquarium.id);
  
    // Supprimer le poisson créé après le test
    await fishRepo.deleteById(res.body.id);
  
    // Supprimer l'aquarium créé après le test
    await aquariumRepo.deleteById(aquarium.id);
  });

  it('updates a fish in an aquarium via PATCH /aquariums/{id}/fish', async () => {
    // Créer un aquarium et un poisson pour le test
    const aquarium = await aquariumRepo.create({ name: 'Test Aquarium' });
    const fish = await fishRepo.create({ name: 'Goldfish', aquariumId: aquarium.id });

    const updatedFishData = { id: fish.id, name: 'Updated Goldfish' };
    // Effectuer la requête HTTP PATCH pour mettre à jour le poisson dans l'aquarium
    await client
      .patch(`/aquariums/${aquarium.id}/fish`)
      .send(updatedFishData)
      .expect(200);

    // Vérifier que le poisson a été correctement mis à jour dans le repository
    const updatedFish = await fishRepo.findById(fish.id);
    expect(updatedFish).to.containEql(updatedFishData);

    // Supprimer le poisson créé après le test
    await fishRepo.deleteById(fish.id);
  
    // Supprimer l'aquarium créé après le test
    await aquariumRepo.deleteById(aquarium.id);
  });

  it('deletes a fish from an aquarium via DELETE /aquariums/{id}/fish', async () => {
    // Créer un aquarium et un poisson pour le test
    const aquarium = await aquariumRepo.create({ name: 'Test Aquarium' });

    // Effectuer la requête HTTP DELETE pour supprimer le poisson de l'aquarium
    await client.del(`/aquariums/${aquarium.id}/fish`).expect(200);
  });

  it('gets all fishes from an aquarium via GET /aquariums/{id}/fish', async () => {
    // Créer un aquarium et plusieurs poissons pour le test
    const aquarium = await aquariumRepo.create({ name: 'Test Aquarium Get Fish' });
    await fishRepo.createAll([
      { name: 'Goldfish', aquariumId: aquarium.id },
      { name: 'Koi', aquariumId: aquarium.id },
    ]);

    // Effectuer la requête HTTP GET pour récupérer tous les poissons de l'aquarium
    const res = await client.get(`/aquariums/${aquarium.id}/fish`).expect(200);

    // Vérifier que la réponse contient tous les poissons créés
    expect(res.body.length).to.equal(2); // Assurez-vous d'adapter selon vos données

    
    // Supprimer l'aquarium créé après le test
    await aquariumRepo.deleteById(aquarium.id);
  });
});
