import {Client, expect} from '@loopback/testlab';
import {AquafunServerApplication} from '../..';
import {setupApplication} from './test-helper'; 
import { Aquarium } from '../../models';
import {AquariumRepository} from '../../repositories';

describe('AquariumController', () => {
  let app: AquafunServerApplication;
  let client: Client;
  let aquariumRepo: AquariumRepository;

  before('setupApplication', async () => {
    ({app, client} = await setupApplication());
    aquariumRepo = await app.getRepository(AquariumRepository);
  });

  after(async () => {
    await app.stop();
  });

  it('invokes POST /aquariums with valid name', async () => {
    const aquariumData = {name: 'Aquariumotor'};
    const res = await client
      .post('/aquariums')
      .send(aquariumData)
      .expect(200);
    expect(res.body).to.containEql(aquariumData);

    // Récupérer l'aquarium créé depuis la réponse
    const createdAquarium: Aquarium = res.body;

    // Supprimer l'aquarium créé après le test
    await aquariumRepo.deleteById(createdAquarium.id);

  });

  it('invokes POST /aquariums with name and id', async () => {
    const aquariumData = {name: 'Aquariumiseur', id: 1};
    await client
      .post('/aquariums')
      .send(aquariumData)
      .expect(422);
  });

  it('invokes POST /aquariums with name and capacity', async () => {
    const aquariumData = {name: 'Aquariumiste', capacity: 50};
    await client
      .post('/aquariums')
      .send(aquariumData)
      .expect(422);
  });

  it('creates an aquarium directly and verifies count via GET /aquariums/count', async () => {
    // Ajouter une donnée directement via le repository
    const aquariumData = { name: 'Test Aquarium' };
    const createdAquarium = await aquariumRepo.create(aquariumData);
  
    // Vérifier que le compteur retourne 1 avec GET /aquariums/count
    const res = await client
      .get('/aquariums/count')
      .expect(200);
    expect(res.body.count).to.equal(1);
  
    // Supprimer l'aquarium créé
    await aquariumRepo.deleteById(createdAquarium.id);
  
    // Vérifier que la donnée a bien été supprimée
    const resAfterDelete = await client
      .get('/aquariums/count')
      .expect(200);
    expect(resAfterDelete.body.count).to.equal(0);
  
  });

  it('returns array of Aquariums with Fishes', async () => {
    const res = await client.get('/aquariums').expect(200);

    // Vérifier que la réponse est un tableau
    expect(res.body).to.be.Array();

    // Vérifier la structure de chaque aquarium dans le tableau
    for (const aquarium of res.body) {
      expect(aquarium).to.have.properties('id', 'name', 'fishes');
      expect(aquarium.fishes).to.be.Array();

      // Vérifier la structure de chaque poisson dans l'aquarium
      for (const fish of aquarium.fishes) {
        expect(fish).to.have.properties('id', 'name', 'aquariumId');
      }
    }
  });

  it('updates aquariums matching the filter', async () => {
    // Créer plusieurs aquariums pour le test
    await aquariumRepo.create({ name: 'Test Aquarium 1' });
    await aquariumRepo.create({ name: 'Test Aquarium 2' });
  
    // Définir le filtre pour mettre à jour certains aquariums
    const filter = { where: { name: { inq: ['Test Aquarium 1', 'Test Aquarium 2'] } } };
  
    const filterUpdate = { where: { name: "Updated Aquarium Name" } };
    // Données de mise à jour partielle
    const updatedData = { name: "Updated Aquarium Name" };
  
    // Appeler la méthode PATCH /aquariums avec les données de mise à jour et le filtre
    const res = await client
      .patch('/aquariums')
      .send(updatedData)
      .query(filter)
      .expect(200);
  
    // Vérifier que la réponse contient le nombre d'aquariums mis à jour attendu
    expect(res.body.count).to.equal(2);
  
    // Vérifier que les aquariums ont été correctement mis à jour dans la base de données
    const updatedAquariums = await aquariumRepo.find(filterUpdate);
    // Vérifier que les noms des aquariums sont mis à jour correctement
    for (const aquarium of updatedAquariums) {
      expect(aquarium.name).to.equal(updatedData.name);
    }
  
    // Supprimer les aquariums mis à jour
    for (const aquarium of updatedAquariums) {
      await aquariumRepo.deleteById(aquarium.id);
    }
  
  });
  
});
