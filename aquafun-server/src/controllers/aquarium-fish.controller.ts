import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Aquarium,
  Fish,
} from '../models';
import {AquariumRepository} from '../repositories';

export class AquariumFishController {
  constructor(
    @repository(AquariumRepository) protected aquariumRepository: AquariumRepository,
  ) { }

  @get('/aquariums/{id}/fish', {
    responses: {
      '200': {
        description: 'Array of Aquarium has many Fish',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Fish)},
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: number,
    @param.query.object('filter') filter?: Filter<Fish>,
  ): Promise<Fish[]> {
    return this.aquariumRepository.fishes(id).find(filter);
  }

  @post('/aquariums/{id}/fish', {
    responses: {
      '200': {
        description: 'Aquarium model instance',
        content: {'application/json': {schema: getModelSchemaRef(Fish)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof Aquarium.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Fish, {
            title: 'NewFishInAquarium',
            exclude: ['id'],
            optional: ['aquariumId']
          }),
        },
      },
    }) fish: Omit<Fish, 'id'>,
  ): Promise<Fish> {
    return this.aquariumRepository.fishes(id).create(fish);
  }

  @patch('/aquariums/{id}/fish', {
    responses: {
      '200': {
        description: 'Aquarium.Fish PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Fish, {partial: true}),
        },
      },
    })
    fish: Partial<Fish>,
    @param.query.object('where', getWhereSchemaFor(Fish)) where?: Where<Fish>,
  ): Promise<Count> {
    return this.aquariumRepository.fishes(id).patch(fish, where);
  }

  @del('/aquariums/{id}/fish', {
    responses: {
      '200': {
        description: 'Aquarium.Fish DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: number,
    @param.query.object('where', getWhereSchemaFor(Fish)) where?: Where<Fish>,
  ): Promise<Count> {
    return this.aquariumRepository.fishes(id).delete(where);
  }
}
