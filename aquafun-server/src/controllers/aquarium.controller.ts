import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {Aquarium} from '../models';
import {AquariumRepository} from '../repositories';

export class AquariumController {
  constructor(
    @repository(AquariumRepository)
    public aquariumRepository: AquariumRepository,
  ) {}

  @post('/aquariums')
  @response(200, {
    description: 'Aquarium model instance',
    content: {'application/json': {schema: getModelSchemaRef(Aquarium)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Aquarium, {
            title: 'NewAquarium',
            exclude: ['id'],
          }),
        },
      },
    })
    aquarium: Omit<Aquarium, 'id'>,
  ): Promise<Aquarium> {
    return this.aquariumRepository.create(aquarium);
  }

  @get('/aquariums/count')
  @response(200, {
    description: 'Aquarium model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(@param.where(Aquarium) where?: Where<Aquarium>): Promise<Count> {
    return this.aquariumRepository.count(where);
  }

  @get('/aquariums')
  @response(200, {
    description: 'Array of Aquarium model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Aquarium, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Aquarium) filter?: Filter<Aquarium>,
  ): Promise<Aquarium[]> {
    return this.aquariumRepository.find(filter);
  }

  @patch('/aquariums')
  @response(200, {
    description: 'Aquarium PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Aquarium, {partial: true}),
        },
      },
    })
    aquarium: Aquarium,
    @param.where(Aquarium) where?: Where<Aquarium>,
  ): Promise<Count> {
    return this.aquariumRepository.updateAll(aquarium, where);
  }

  @get('/aquariums/{id}')
  @response(200, {
    description: 'Aquarium model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Aquarium, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: number,
    @param.filter(Aquarium, {exclude: 'where'})
    filter?: FilterExcludingWhere<Aquarium>,
  ): Promise<Aquarium> {
    return this.aquariumRepository.findById(id, filter);
  }

  @patch('/aquariums/{id}')
  @response(204, {
    description: 'Aquarium PATCH success',
  })
  async updateById(
    @param.path.string('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Aquarium, {partial: true}),
        },
      },
    })
    aquarium: Aquarium,
  ): Promise<void> {
    await this.aquariumRepository.updateById(id, aquarium);
  }

  @put('/aquariums/{id}')
  @response(204, {
    description: 'Aquarium PUT success',
  })
  async replaceById(
    @param.path.string('id') id: number,
    @requestBody() aquarium: Aquarium,
  ): Promise<void> {
    await this.aquariumRepository.replaceById(id, aquarium);
  }

  @del('/aquariums/{id}')
  @response(204, {
    description: 'Aquarium DELETE success',
  })
  async deleteById(@param.path.string('id') id: number): Promise<void> {
    await this.aquariumRepository.deleteById(id);
  }
}
