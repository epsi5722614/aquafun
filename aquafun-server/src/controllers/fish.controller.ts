import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {Fish} from '../models';
import {FishRepository} from '../repositories';

export class FishController {
  constructor(
    @repository(FishRepository)
    public fishRepository: FishRepository,
  ) {}

  @post('/fish')
  @response(200, {
    description: 'Fish model instance',
    content: {'application/json': {schema: getModelSchemaRef(Fish)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Fish, {
            title: 'NewFish',
            exclude: ['id'],
          }),
        },
      },
    })
    fish: Omit<Fish, 'id'>,
  ): Promise<Fish> {
    return this.fishRepository.create(fish);
  }

  @get('/fishes/count')
  @response(200, {
    description: 'Fish model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(@param.where(Fish) where?: Where<Fish>): Promise<Count> {
    return this.fishRepository.count(where);
  }

  @get('/fishes')
  @response(200, {
    description: 'Array of Fish model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Fish, {includeRelations: true}),
        },
      },
    },
  })
  async find(@param.filter(Fish) filter?: Filter<Fish>): Promise<Fish[]> {
    return this.fishRepository.find(filter);
  }

  @patch('/fishes')
  @response(200, {
    description: 'Fish PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Fish, {partial: true}),
        },
      },
    })
    fish: Fish,
    @param.where(Fish) where?: Where<Fish>,
  ): Promise<Count> {
    return this.fishRepository.updateAll(fish, where);
  }

  @get('/fishes/{id}')
  @response(200, {
    description: 'Fish model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Fish, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Fish, {exclude: 'where'}) filter?: FilterExcludingWhere<Fish>,
  ): Promise<Fish> {
    return this.fishRepository.findById(id, filter);
  }

  @patch('/fishes/{id}')
  @response(204, {
    description: 'Fish PATCH success',
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Fish, {partial: true}),
        },
      },
    })
    fish: Fish,
  ): Promise<void> {
    await this.fishRepository.updateById(id, fish);
  }

  @put('/fishes/{id}')
  @response(204, {
    description: 'Fish PUT success',
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() fish: Fish,
  ): Promise<void> {
    await this.fishRepository.replaceById(id, fish);
  }

  @del('/fishes/{id}')
  @response(204, {
    description: 'Fish DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.fishRepository.deleteById(id);
  }
}
