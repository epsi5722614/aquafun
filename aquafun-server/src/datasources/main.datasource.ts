import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';

const config = {
  name: 'Main',
  connector: 'mysql',
  host: process.env.DATABASE_HOST,
  port: 3306,
  user: process.env.DATABASE_USERNAME,
  password: process.env.DATABASE_PASSWORD,
  database: 'aquafun',
};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class MainDataSource
  extends juggler.DataSource
  implements LifeCycleObserver
{
  static dataSourceName = 'Main';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.Main', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
