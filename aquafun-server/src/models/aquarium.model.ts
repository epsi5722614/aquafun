import {Entity, model, property, hasMany} from '@loopback/repository';
import {Fish} from './fish.model';

@model()
export class Aquarium extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @hasMany(() => Fish)
  fishes: Fish[];

  constructor(data?: Partial<Aquarium>) {
    super(data);
  }
}

export interface AquariumRelations {
  // describe navigational properties here
}

export type AquariumWithRelations = Aquarium & AquariumRelations;
