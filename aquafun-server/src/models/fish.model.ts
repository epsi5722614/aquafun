import {Entity, model, property} from '@loopback/repository';

@model()
export class Fish extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'number',
  })
  aquariumId?: number;

  constructor(data?: Partial<Fish>) {
    super(data);
  }
}

export interface FishRelations {
  // describe navigational properties here
}

export type FishWithRelations = Fish & FishRelations;
