import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {MainDataSource} from '../datasources';
import {Aquarium, AquariumRelations, Fish} from '../models';
import {FishRepository} from './fish.repository';

export class AquariumRepository extends DefaultCrudRepository<
  Aquarium,
  typeof Aquarium.prototype.id,
  AquariumRelations
> {

  public readonly fishes: HasManyRepositoryFactory<Fish, typeof Aquarium.prototype.id>;

  constructor(@inject('datasources.Main') dataSource: MainDataSource, @repository.getter('FishRepository') protected fishRepositoryGetter: Getter<FishRepository>,) {
    super(Aquarium, dataSource);
    this.fishes = this.createHasManyRepositoryFactoryFor('fishes', fishRepositoryGetter,);
    this.registerInclusionResolver('fishes', this.fishes.inclusionResolver);
  }
}
