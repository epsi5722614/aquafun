import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {MainDataSource} from '../datasources';
import {Fish, FishRelations} from '../models';

export class FishRepository extends DefaultCrudRepository<
  Fish,
  typeof Fish.prototype.id,
  FishRelations
> {
  constructor(
    @inject('datasources.Main') dataSource: MainDataSource,
  ) {
    super(Fish, dataSource);
  }
}
